# defaultErrorPage

Project providing default error pages that can be used for HTTP errors like 502 Gateway Timeout etc

Design based on template from [Saransh Sinha](https://codepen.io/saransh/pen/aezht)

## Sample
![image.png](./image.png)
